package com.epam.Model;

public interface Model {
    void setMobileName(String mobileName);
    String getMobileName();
    String getMobileIMEI();
    void setMobileIMEI(String mobileIMEI);
    boolean isSIMInPhone();
    void setMyNumber(String myNumber);
    String getMyNumber();
    void addNumberToNumberBook(int newNumberContact,String newContactName)throws Exception;
    void deleteNumberFromNumberBook()throws Exception;
}
