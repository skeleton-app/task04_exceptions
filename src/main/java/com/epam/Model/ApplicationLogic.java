package com.epam.Model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class ApplicationLogic implements Model {
    private HashMap<String,String> numberNumberBook;
    private HashMap<String,String> nameNumberBook;
    private Scanner scanKeyBoard = new Scanner(System.in);
    private static int numberBookCounter=1;
    private String mobileName;
    private String mobileIMEI;
    private boolean isSIMCardInPhone;
    private String myNumber;
    MobilePhone Iphone6s = new MobilePhone();

    @Override
    public void setMobileName(String mobileName){
        Iphone6s.setMobileName(mobileName);
    }
    @Override
    public String getMobileName(){
       return Iphone6s.getMobileName();
    }

    @Override
    public String getMobileIMEI(){
     return Iphone6s.getMobileIMEI();
    }
    @Override
    public void setMobileIMEI(String mobileIMEI){
        Iphone6s.setMobileIMEI(mobileIMEI);
    }
    @Override
    public boolean isSIMInPhone(){
        return Iphone6s.isSIMInPhone();
    }
    @Override
    public void setMyNumber(String myNumber){
        Iphone6s.setMyNumber(myNumber);
    }
    @Override
    public String getMyNumber(){
        return Iphone6s.getMyNumber();
    }
    @Override
    public void addNumberToNumberBook(int newNumberContact,String newContactName)throws Exception{
        Iphone6s.addNumberToNumberBook(newNumberContact,newContactName);
    }
    @Override
    public void deleteNumberFromNumberBook()throws Exception{
        Iphone6s.deleteNumberFromNumberBook();
    }

    class MobilePhone{
        MobilePhone(String mobileName,String mobileIMEI,boolean isSIMCardInPhone,String myNumber){
            ApplicationLogic.this.mobileName = mobileName;
            ApplicationLogic.this.mobileIMEI = mobileIMEI;
            ApplicationLogic.this.isSIMCardInPhone = isSIMCardInPhone;
            ApplicationLogic.this.myNumber = myNumber;
        }
        MobilePhone(){}

        void setMobileName(String mobileName){
            ApplicationLogic.this.mobileName = mobileName;
        }
        String getMobileName(){
            return ApplicationLogic.this.mobileName;
        }
        void setMobileIMEI(String mobileIMEI){
            ApplicationLogic.this.mobileIMEI = mobileIMEI;
        }
        String getMobileIMEI(){
            return ApplicationLogic.this.mobileIMEI;
        }
        boolean isSIMInPhone(){
            return ApplicationLogic.this.isSIMCardInPhone;
        }

        void setMyNumber(String myNumber){
            ApplicationLogic.this.myNumber = myNumber;
        }
        String getMyNumber(){
               return ApplicationLogic.this.myNumber;
        }

        void addNumberToNumberBook(int newNumberContact,String newContactName)throws Exception{
            numberNumberBook = new LinkedHashMap<String, String>();
            nameNumberBook = new LinkedHashMap<String, String>();
            numberBookCounter++;
            String key = String.valueOf(numberBookCounter);
            try {
                numberNumberBook.put(key, String.valueOf(newNumberContact));
                nameNumberBook.put(key, newContactName);
            }
            catch (ClassCastException e){
                System.out.println("Cast Exception!!!"+e);
                throw new Exception("Problem with data types");
            }
            catch (NullPointerException nullPointer){
                System.out.println("Null Pointer Exception");
                throw new Exception("Can not use null object");
            }
            catch (ExceptionInInitializerError error){
                System.out.println("Exception In Initializer Error!!!"+error);
            }
        }
        void deleteNumberFromNumberBook()throws Exception{
            System.out.println("Choose how to find number you wanna delete:");
            System.out.println("1 - Search by number");
            System.out.println("2 - Search by Last Name of person");
            String in = scanKeyBoard.nextLine();
            String key;
            if(in.equals("1")){
                System.out.println("Input the number");
                String number = scanKeyBoard.nextLine();
                key = searchByNumber(number);
            }
            if(in.equals("2")){
                System.out.println("Input the Last Name");
                String lastName = scanKeyBoard.nextLine();
                key = searchByLastName(lastName);
            }
            else{
                key = String.valueOf(0);
            }
            try {
                numberNumberBook.remove(key);
                nameNumberBook.remove(key);
            }
            catch (ClassCastException e){
                System.out.println("Cast Exception!!!"+e);
                throw new Exception("Problem with data types");
            }
            catch (NullPointerException nullPointer){
                System.out.println("Null Pointer Exception");
                throw new Exception("Can not use null object");
            }
            catch (ExceptionInInitializerError error){
                System.out.println("Exception In Initializer Error!!!"+error);
            }
        }

        private String searchByLastName(String lastName){
            String key;
            for (int i=1;i<=numberBookCounter;i++) {
                try {
                    if (lastName.equals(nameNumberBook.get(i))) {
                        key = String.valueOf(i);
                        return key;
                    }
                }catch (Exception e){
                    System.out.println("Nothing to delete"+e);
                }
            }
            return null;
        }

        private String searchByNumber(String number) {
            String key;
            for (int i=1;i<=numberBookCounter;i++) {
                try {
                    if (number.equals(numberNumberBook.get(i))) {
                        key = String.valueOf(i);
                        return key;
                    }
                }catch (Exception e){
                    System.out.println("Nothing to delete"+e);
                }
            }
            return null;
        }

    }
}
