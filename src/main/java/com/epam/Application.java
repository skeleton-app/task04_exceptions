package com.epam;

import com.epam.Controller.Controller;
import com.epam.Controller.ControllerImpl;
import com.epam.Model.ApplicationLogic;
import com.epam.Model.Model;
import com.epam.Wiewer.View;

public class Application {
    public static void main(String[] args) {
        Model model = new ApplicationLogic();
        Controller controller = new ControllerImpl(model);
        new View(controller).show();
    }
}
