package com.epam.Wiewer;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import com.epam.Controller.Controller;

public class View {
        private Map<String,String> menu;
        private Map<String,Printable> methodsMenu;
        private static Scanner input = new Scanner(System.in);
        private Controller controller;
        public View(Controller controller) {
            this.controller = controller;
            menu = new LinkedHashMap<String, String>();
            menu.put("1", "1 - Set Mobile Name");
            menu.put("2", "2 - Get Mobile Name");
            menu.put("3", "3 - Is SIM In Phone?");
            menu.put("4", "4 - Set My Number");
            menu.put("5", "5 - Get My Number");
            menu.put("6", "6 - Add Number ToNumber Book");
            menu.put("7", "7 - Delete Number From Book");

            methodsMenu = new LinkedHashMap<>();
            methodsMenu.put("1", this::setMobileName);
            methodsMenu.put("2", this::getMobileName);
            methodsMenu.put("3", this::isSIMInPhone);
            methodsMenu.put("4", this::setMyNumber);
            methodsMenu.put("5", this::getMyNumber);
            methodsMenu.put("6", this::addNumberToNumberBook);
            methodsMenu.put("7", this::deleteNumberFromNumberBook);
        }


        private void setMobileName() {
            System.out.println("Enter mobile name:");
            String mobileName = input.nextLine();
            controller.setMobileName(mobileName);
        }
        private void getMobileName(){
            controller.getMobileName();
        }
        private void isSIMInPhone(){
            if(controller.isSIMInPhone()){
                System.out.println("Yes, SIM-card is in phone");
            }
            else{
                System.out.println("No, SIM-card is absent");
            }

        }
        private void setMyNumber(){
            System.out.println("Input your number");
            String myNumber = input.nextLine();
            controller.setMyNumber(myNumber);
        }
        private void getMyNumber(){
            controller.getMyNumber();
        }
        private void addNumberToNumberBook(){
            System.out.println("Input person`s First Name and Last Name");
            String contactName = input.nextLine();
            System.out.println("Input person`s phone number");
            int numberContact = input.nextInt();

            try {
                controller.addNumberToNumberBook(numberContact,contactName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void deleteNumberFromNumberBook(){
            try {
                controller.deleteNumberFromNumberBook();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private void outputMenu(){
            System.out.println("\nMENU:");
            for(String str: menu.values()){
                System.out.println(str);
            }
        }
        public void show(){
            String keyMenu;
            do{
                this.outputMenu();
                System.out.println("Press, number or key < Q >");
                keyMenu = input.nextLine().toUpperCase();
                try{
                    methodsMenu.get(keyMenu).print();
                }catch (Exception e){
                    System.out.println("ERROR. " + e);
                }
            }
            while(!keyMenu.equals("Q"));
        }
}
