package com.epam.Controller;

import com.epam.Model.Model;

import java.util.Map;

public class ControllerImpl implements  Controller {
public Model model;
public ControllerImpl(Model model){
    this.model = model;
}
    @Override
    public void setMobileName(String mobileName){
    model.setMobileName(mobileName);
    }
    @Override
    public String getMobileName(){
    return model.getMobileName();
    }
    @Override
    public String getMobileIMEI(){
    return model.getMobileIMEI();
    }
    @Override
    public void setMobileIMEI(String mobileIMEI){
    model.setMobileIMEI(mobileIMEI);
    }
    @Override
    public boolean isSIMInPhone(){
    return model.isSIMInPhone();
    }
    @Override
    public void setMyNumber(String myNumber){
    model.setMyNumber(myNumber);
    }
    @Override
    public String getMyNumber(){
    return model.getMyNumber();
    }
    @Override
    public void addNumberToNumberBook(int newNumberContact,String newContactName)throws Exception{
    model.addNumberToNumberBook(newNumberContact,newContactName);
    }
    @Override
    public void deleteNumberFromNumberBook()throws Exception{
    model.deleteNumberFromNumberBook();
    }
}
